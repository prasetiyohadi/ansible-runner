# Ansible Runner

A container image that contains installation of [Ansible Runner](https://pypi.org/project/ansible-runner/) based on configuration in [ansible/ansible-runner](https://github.com/ansible/ansible-runner).

Ansible Runner is a tool and python library that helps when interfacing with Ansible directly or as part of another system whether that be through a container image interface, as a standalone tool, or as a Python module that can be imported. The goal is to provide a stable and consistent interface abstraction to Ansible.

For the latest documentation see: [https://ansible-runner.readthedocs.io](https://ansible-runner.readthedocs.io/en/latest/)
